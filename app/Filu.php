<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filu extends Model
{
    protected $fillable = ['name' , 'category', 'file_url' ];
}
