<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use DataTables;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:staff');
    }
    
    public function getClients(){
        $orders = Order::where(['order_status' => '2'])->get();
        $orders2 = $orders;
        $users = [];

        foreach($orders as $order){
            $count = 0;
            $total = 0;
            $phone = $order->user_phone;
            foreach($orders2 as $order2){
                if($order2->user_phone == $phone){
                    $count++;
                    $number = str_replace(',', '', $order->total_price);
                    $total = $total + (int) $number;
                }                
            }   

            $check = 0;
            foreach($users as $user){
                if($user->phone == $order->user_phone){
                    $check = 1;
                }
            }

             if($count > 0 && $count < 5 ){
                $stars = 1;
            }if($count >= 5 && $count < 10){
                $stars = 2;
            }if($count >= 10 && $count < 15){
                $stars = 3;
            }if($count >= 15 && $count < 20){
                $stars = 4;
            }if($count >= 20 && $count < 30){
                $stars = 5;
            }

            if($check == 0){
                $user = new User();
                $user->name = $order->user_name;
                $user->phone = $order->user_phone;
                $user->email = $order->user_email;
                $user->orders = $count;
                $user->total = $total;
                $user->stars = $stars;

                array_push($users, $user);    
            }            
        }

            return DataTables::of($users)
            ->editColumn('stars', function ($user) {
                if($user->stars == 5){
                    return '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>';
                }
                if($user->stars == 4){
                    return '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>';
                }
                if($user->stars == 3){
                    return '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>';
                }
                if($user->stars == 2){
                    return '<span class="fa fa-star checked"></span>'.
                    '<span class="fa fa-star checked"></span>';
                }
                if($user->stars == 1){
                    return '<span class="fa fa-star checked"></span>';
                }
            })
            ->rawColumns(['stars'])
            ->make(true);
    }

    public function index(){
        return view('clients.clients');
    }
}
