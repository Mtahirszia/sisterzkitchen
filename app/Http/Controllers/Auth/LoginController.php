<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Session;
use Redirect;

use App\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
        {
            if($request->check == "Admin"){
                $this->validateLogin($request);

                // If the class is using the ThrottlesLogins trait, we can automatically throttle
                // the login attempts for this application. We'll key this by the username and
                // the IP address of the client making these requests into this application.
                if ($this->hasTooManyLoginAttempts($request)) {
                    $this->fireLockoutEvent($request);
    
                    return $this->sendLockoutResponse($request);
                }
    
                if ($this->attemptLogin($request)) {
                    return $this->sendLoginResponse($request);
                }
    
                // If the login attempt was unsuccessful we will increment the number of attempts
                // to login and redirect the user back to the login form. Of course, when this
                // user surpasses their maximum number of attempts they will get locked out.
                $this->incrementLoginAttempts($request);
    
                return $this->sendFailedLoginResponse($request);
            }else{
                if (Auth::guard('staff')->attempt(['email' => $request->email, 'password' => $request->password])) {                  
                    if(Auth::guard('staff')->check()){
                        if(Auth::guard('staff')->user()->block == 1){
                            return "User is Blocked";                        
                        }else{
                            Auth::guard('staff')->login(Auth::guard('staff')->user(), true);
                            return redirect()->route('home');                        
                        }
                    }
                } else {
                    return $this->sendFailedLoginResponse($request);

                }
            }
        }

    protected function authenticated(Request $request, $user){

        if ( Auth::check() ) {
            return redirect()->route('home');
        }
    }

    public function logout(Request $request)
    {

        $this->guard()->logout();

        $request->session()->invalidate();

        Auth::logout(); 
        Auth::guard('staff')->logout(); 
        Session::flush();

        return $this->loggedOut($request) ?: Redirect::route('login');        
    }
}
