<?php

namespace App\Http\Controllers;

use Validator;
use DataTables;
use App\Staff;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('auth:staff');
    }

    public function getStaff(){
        $staff = Staff::get();
       
        foreach($staff as $single){
            $single->block = Staff::block($single->block);
            $role = Role::where(['id' => $single->role])->first();
            $single->role = $role->name;
        }
    

        return DataTables::of($staff)
        ->addColumn('action', function ($id) {
            return '<a href="'. route("staff.edit" , $id->id) .'" class="text-danger"><i class="fa fa-edit"></i></a>
            <a class="text-danger" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function index()
    {
        return view('staff.staff');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        if(count($roles) > 0){
            return view('staff.addStaff' , compact('roles'));
        }else{
            return redirect('staff')->with('errorUsers', 'Please Add User Roles First');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:200', 
            'mobile' => 'required|digits:11', 
            'gender' => 'required',
            'age' => 'required',
            'cnic' => 'required|digits:13',
            'address' => 'required',
            'email' => 'required|email|unique:staff,email',
            'password' => 'required',
            'block' => 'required',
            'role' => 'required'
            ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $staff = Staff::create([
            'name' => $request->name , 
            'mobile' => $request->mobile , 
            'gender' => $request->gender , 
            'age' => $request->age ,
            'cnic' => $request->cnic ,
            'address' => $request->address ,
            'email' => $request->email ,
            'password' => Hash::make($request->password) ,
            'block' => $request->block ,
            'role' => $request->role ,
        ]);

        $role = Role::where(['id' => $request->role])->first();
        $staff->save();
        $staff->assignRole($role->name);
        return redirect('staff')->with('message', 'Staff Added Successfully');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $staff = Staff::where(['id' => $id])->first();
        return view('staff.editStaff' , compact('staff' , 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:200', 
            'mobile' => 'required|digits:11', 
            'gender' => 'required',
            'age' => 'required',
            'role' => 'required',
            'cnic' => 'required|digits:13',
            'email' => 'required|email|unique:staff,email,'.$request->id.',id',
            'password' => 'required',
            'address' => 'required',
            'block' => 'required',
            'role' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator);
        }

        $staff = Staff::where(['id' => $request->id])->first();

        $role = Role::where(['id' => $staff->role])->first();
        $staff->removeRole($role->name);

        $staff->name = $request->name;
        $staff->mobile = $request->mobile;
        $staff->gender = $request->gender;
        $staff->age = $request->age;
        $staff->cnic = $request->cnic;
        $staff->address = $request->address;
        $staff->block = $request->block;
        $staff->email = $request->email;
        $staff->password = Hash::make($request->password);
        $staff->role = $request->role;

        $role = Role::where(['id' => $request->role])->first();
        $staff->save();
        $staff->assignRole($role->name);
        return redirect('staff')->with('message', 'Staff Edit Successfully');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        Staff::where(['id' => $id])->delete();   
        return redirect()->back()->with('message', 'Staff Deleted Successfully');;
    }
}
