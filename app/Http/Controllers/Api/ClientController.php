<?php

namespace App\Http\Controllers\Api;

use App\Client;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    public function addClient(Request $request){

        $cl = Client::where(['phone' => $request->phone])->first();
        if(!$cl){

            $client = new Client();

            $client->name = $request->name;
            $client->email = $request->email;
            $client->phone = $request->phone;
            $client->address = $request->address; 
            $client->android_id = $request->android_id; 
            $client->token = $client->createToken('MyApp')->accessToken;
            $client->verified = 1;
    
            $client->save();
    
        }
        return response()->json(array('success' => true), 200);        

    }

    public function getClient($android_id){
        $client = Client::where(['android_id' => $android_id])->first();
        if($client){
            if($client->verified == true){
                $client->verified = true;
            }else{
                $client->verified = false;
            }
            return response()->json($client, 200); 
        }else{
            return response()->json(array('status' => false), 403);
        }
    }
}
