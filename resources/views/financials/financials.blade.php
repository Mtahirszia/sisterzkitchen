@extends('layouts.main')

@section('title' , 'Financials Report')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .ml-00{
          margin-left:1px !important;
      }
     .active-yr, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-mn, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
        .text-primary{
            color:#e74a3b !important;            
        }
      .dates-row{
          z-index:9999;
          position:relative;
      }
      #salesTable_filter{
        display:none;
      }
      #debit{
        background:#e74a3b;
        color:white;
      }
      #credit{
        background:#cdffcc;
        color:green;
      }

      div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
            margin-right: 5px;
        }
        .dt-button{
          float:right;
          z-index:9999;
          position:relative;
          margin-bottom:20px;
        }
        .table-responsive{
            margin-top:-30px;
        }
      @media screen and (max-width: 600px) {
        .btn-group{
          display:inline-block;
        }
        .mn{
          margin-top:10px;
        }
        .table-responsive{
            margin-top:5px;
        }
        .exp-btn {
            display:none;
        }
        .table-responsive{
            margin-top:5px;
        }
        .dt-button{
            margin-top:5px;
            margin-bottom:5px;
        }
        label {
            display: inline-block;
            margin-bottom: .5rem;
            float: right;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Sales</h1>

        <div class="row">
            <div class="col-md-6 col-12">
                <div class="btn-group" id="years">
                    <button type="button" id="2020" class="yr btn btn-danger">2020</button>
                    <button type="button" id="2021" class="ml-00 yr btn btn-danger">2021</button>
                    <button type="button" id="2022" class="ml-00 yr btn btn-danger">2022</button>
                    <button type="button" id="2023" class="ml-00 yr btn btn-danger">2023</button>
                </div>
            </div>

            <div class="col-md-6 exp-btn text-right">
                <a href="{{ url('revenues') }}"><button class="btn btn-danger mr-2">Revenues</button></a>
                <a href="{{ url('expenses') }}"><button class="btn btn-danger">Expenses</button></a>
            </div>

            <div class="col-md-12 mt-2">
                <div class="btn-group" id="months">
                    <button type="button" id="1" class="btn mn btn-danger">Jan</button>
                    <button type="button" id="2" class="ml-00 mn btn btn-danger">Feb</button>
                    <button type="button" id="3" class="ml-00 mn btn btn-danger">March</button>
                    <button type="button" id="4" class="ml-00 mn btn btn-danger">April</button>
                    <button type="button" id="5" class="ml-00 mn btn btn-danger">May</button>
                    <button type="button" id="6" class="ml-00 mn btn btn-danger">June</button>
                    <button type="button" id="7" class="ml-00 mn btn btn-danger">July</button>
                    <button type="button" id="8" class="ml-00 mn btn btn-danger">Aug</button>
                    <button type="button" id="9" class="ml-00 mn btn btn-danger">Sep</button>
                    <button type="button" id="10" class="ml-00 mn btn btn-danger">Oct</button>
                    <button type="button" id="11" class="ml-00 mn btn btn-danger">Nov</button>
                    <button type="button" id="12" class="ml-00 mn btn btn-danger">Dec</button>
                </div>
            </div>            

        </div>


          <!-- DataTales Example -->
          <div class="card shadow mt-2 mb-4">
            <div class="card-body">
              <div class="row dates-row">
                    <div class="col-md-6 col-12">
                    <div class="row">
                        <div class="col-md-6 mt-2 col-12">
                            <input type="date" class="form-control" id="start_date">
                        </div>
                        <div class="col-md-6 mt-2 col-12">
                            <input type="date" class="form-control" id="end_date">
                        </div>
                    </div>
                    </div>
              </div>
              
              <div class="table-responsive">
                <table class="table table-bordered" id="salesTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Title</th>
                      <th>Debit</th>
                      <th>Credit</th>
                      <th>Balance</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Date</th>
                      <th>Title</th>
                      <th>Debit</th>
                      <th>Credit</th>
                      <th>Balance</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the Category.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>  
  <!-- <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>   -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>   -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>  

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

        // Selection Area
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var start_date = null;
        var end_date = null;
        $('#'+year).addClass('active-yr');
        $('#'+month).addClass('active-mn');

        // Add active class to the current button (highlight it)
        var header = document.getElementById("years");
        var btns = header.getElementsByClassName("yr");
        for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active-yr");
        current[0].className = current[0].className.replace(" active-yr", "");
        this.className += " active-yr";
        });
        }


                // Add active class to the current button (highlight it)
        var header = document.getElementById("months");
        var btns = header.getElementsByClassName("mn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-mn");
          current[0].className = current[0].className.replace(" active-mn", "");
          this.className += " active-mn";
          });
        }


        $('.yr').click(function(){
            year = this.id;
            getData();
        });

        $('.mn').click(function(){
            month = this.id;
            getData();
        });

       function delete_click(clicked_id){
          $('#deleteModel').attr("href","{{url('deleteCategory')}}/"+clicked_id)
          $('#deleteCategoryModal').modal('show');
        }

        $('#salesTable').DataTable({
              "processing":true,
              "serverside":true,
              "bPaginate": false,
              "bInfo" : false,
              "pageLength": 50,
              "ajax":"{{url('ajaxFinancials')}}/"+year+"/"+month,
              "columns":[
                {"data" : "date"},
                {"data" : "title"},
                {"data" : "debit"},
                {"data" : "credit"},
                {"data" : "balance"},
              ],
              order:[],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    // if(aData['green'] == false){
                    //     $('td', nRow).css('background-color', '#f8a5a5');
                    //     $('td', nRow).css('color', '#000000')
                    // }else{
                    //   $('td', nRow).css('background-color', '#cdffcc');
                    //   $('td', nRow).css('color', '#000000');                      
                    // }
                },
                dom: 'Bfrtip',
                buttons: [
                    'pdf', 'print'
                ]
            });

        function getData(){
          $('#start_date').val("");
          $('#end_date').val("");
          $('#salesTable').DataTable().ajax.url("{{url('ajaxFinancials')}}/"+year+"/"+month).load();
        }

        $('#start_date').change(function(){
            start_date = this.value;
            getDataByDate();
        });
        $('#end_date').change(function(){
            end_date = this.value;
            getDataByDate();
        });

        function getDataByDate(){
            if(start_date != null && end_date != null){
              $('#salesTable').DataTable().ajax.url("{{url('ajaxFinancialDates')}}/"+start_date+"/"+end_date).load();
            }
        }

        $(".dt-button").addClass("mr-1 btn btn-sm btn-danger");

  </script>
@endsection