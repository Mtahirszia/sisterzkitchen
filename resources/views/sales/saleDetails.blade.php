@extends('layouts.main')

@section('title' , 'Sales')

@section('styles')
    <style>
        .image{
            width:80px;
            height:80px;
        }
        .date{
            color:#ffffff;
            background:#e74a3b;
            padding-right:20px;
            padding-left:20px;
        }
        .details{
            font-size:20px;
        }
        .green{
            background:#e74a3b;
            color:white;
        }
    </style>
@endsection
@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Sale Details </h1>

            <div class="mt-3 mb-3 row details">
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 col-6 text-right">
                                Date : 
                        </div>
                        <div class="col-md-6 col-6 text-right">
                                <span class="date">{{$date}}</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-6 text-right">
                                Total Items
                        </div>
                        <div class="col-md-6 col-6 text-right">
                                <span class="date">{{$items}} Items </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-6 text-right">
                                Sub Total
                        </div>
                        <div class="col-md-6 col-6 text-right">
                                <span class="date">Rs {{$total}} </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-6 text-right">
                                Discount
                        </div>
                        <div class="col-md-6 col-6 text-right">
                                <span class="date">Rs {{$discount}} </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-6 text-right">
                                Delivery
                        </div>
                        <div class="col-md-6 col-6 text-right">
                                <span class="date">Rs {{$delivery}} </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-6 text-right">
                                Total Price
                        </div>
                        <div class="col-md-6 col-6 text-right">
                                <span class="date">Rs {{(int) $total + (int)$delivery - (int)$discount}} </span>
                        </div>
                    </div>

                </div>
            </div>

          <!-- DataTales Example -->
          <div class="card shadow mb-4 mt-2">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="salesTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>Id</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach($products as $product)
                        @if($product->count > 0) <tr class="green"> @else <tr> @endif
                            <td>{{$product->id}}</td>
                            <td><img class="image" src="{{$product->image}}" alt=""></td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->count}}</td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->price * $product->count}}</td>
                        </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

@endsection
