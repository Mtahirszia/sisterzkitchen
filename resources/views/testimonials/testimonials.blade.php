@extends('layouts.main')

@section('title' , 'Sales')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .ml-00{
          margin-left:1px !important;
      }
        .text-primary{
            color:#e74a3b !important;            
        }
        .dates-row{
            z-index:9999;
            position:relative;
        }
      @media screen and (max-width: 600px) {
        .btn-group{
          display:inline-block;
        }
        .mn{
          margin-top:10px;
        }
        .exp-btn {
            text-align:left !important;
            margin-top:20px;
        }
        .add-exp-btn{
            text-align:left !important;
            margin-top:20px;
        }        
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Testimonials
      <span class="float-right"><a href="{{ url('addTestimonial') }}"><button class="btn btn-danger">Add Testimonial</button></a></span></h1>

          <!-- DataTales Example -->
          <div class="card shadow mt-2 mb-4">
            <div class="card-body">
              
              <div class="table-responsive">
                <table class="table table-bordered" id="testimonialTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Author</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Id</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Author</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the Category.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

       function delete_click(clicked_id){
          $('#deleteModel').attr("href","{{url('deleteTestimonial')}}/"+clicked_id)
          $('#deleteCategoryModal').modal('show');
        }

        $('#testimonialTable').DataTable({
              "processing":true,
              "serverside":true,
              "bPaginate": false,
              "bInfo" : false,
              "pageLength": 50,
              "ajax":"{{url('ajaxTestimonials')}}",
              "columns":[
                {"data" : "id"},
                {"data" : "title"},
                {"data" : "description"},
                {"data" : "author"},
                {"data" : "date"},
                {"data" : "action"},
              ],
              order:[0,'desc']
            });

        function getData(){
          $('#testimonialTable').DataTable().ajax.url("{{url('ajaxExpenses')}}/"+year+"/"+month).load();
        }

  </script>
@endsection