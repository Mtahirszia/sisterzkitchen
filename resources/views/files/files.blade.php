@extends('layouts.main')

@section('title' , 'Files')

@section('styles')
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .small-h3{
        font-size:18px;
        font-weight:bold;
      }
      #toggle_event_editing{
        background:white !important;
      }
      .trash {
        cursor:pointer;
      }
      .card-link{
        color:#e74a3b;
        font-weight:bold;
        font-size:24px;
      }
      .card-body ul li a{
        color:#e74a3b !important;
        font-weight:bold;
        font-size:22px;
        padding-left:10px;
      }
      ul{
        padding:0px;
      }
      .trash{
        font-size:22px;
      }
      input[type=checkbox]{
        /* Double-sized Checkboxes */
        -ms-transform: scale(2); /* IE */
        -moz-transform: scale(2); /* FF */
        -webkit-transform: scale(2); /* Safari and Chrome */
        -o-transform: scale(2); /* Opera */
        transform: scale(2);
        padding: 10px;
      }

      @media screen and (max-width: 600px) {
        .card-body ul li a{
          color:#e74a3b;
          font-weight:bold;
          font-size:18px;
        }
        .trash{
          font-size:18px;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
          <!-- DataTales Example -->
          <div class="mt-5 card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-danger">Files 
                <span class="float-right">
                  <button class="btn btn-sm btn-danger" id="download_files">Download Selected Files</button>
                  <a href="{{url('addFile')}}" title="Add File"><button class="btn btn-sm btn-danger">Add File</button></a>
                </span>
               </h4>
            </div>
            <div class="card-body">

            <div id="accordion">

<div class="card">
  <div class="card-header">
    <a class="collapsed card-link" data-toggle="collapse" href="#collapseOne">
      Leasing Contracts
    </a>
  </div>
  <div id="collapseOne" class="collapse" data-parent="#accordion">
    <div class="card-body">
        <ul style="list-style-type:none;">
        @if($leasing_contracts > 0)
        @foreach($files as $file)
            @if($file->category == "Leasing Contracts")
            <li>
                <input class="check_file" type="checkbox" id="{{$file->id}}">
                <a rel="nofollow" id="{{$file->name}}" href="{{ url('/download').'/'.$file->name }}"> {{ $file->name }}</a>
                <span class="float-right"><i onClick="delete_click('{{$file->id}}')" class="trash fa fa-trash text-danger"></i></span>
            </li>
            @endif
        @endforeach
        <br>
        <button onclick="download_all('leasing_contracts')" id="all_leasing_contracts" class="btn btn-sm btn-danger">All Files Download</button><br>
        @else
        No File 
        @endif

        </ul>
    </div>
  </div>
</div>

<div class="card">
  <div class="card-header">
    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
      License Permits
    </a>
  </div>
  <div id="collapseTwo" class="collapse" data-parent="#accordion">
    <div class="card-body">
    <ul style="list-style-type:none;">
    @if($license_permits > 0)
        @foreach($files as $file)
            @if($file->category == "License Permits")
            <li>
                <input class="check_file" type="checkbox" id="{{$file->id}}">
                <a rel="nofollow" id="{{$file->name}}" href="{{ url('/download').'/'.$file->name }}"> {{ $file->name }}</a>
                <span class="float-right"><i onClick="delete_click('{{$file->id}}')" class="trash fa fa-trash text-danger"></i></span>
            </li>
            @endif
        @endforeach
        <br>
        <button onclick="download_all('license_permits')" id="all_license_permits" class="btn btn-sm btn-danger">All Files Download</button>

    @else
    No File 
    @endif

        </ul>
    </div>
  </div>
</div>

<div class="card">
  <div class="card-header">
    <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
      HR
    </a>
  </div>
  <div id="collapseThree" class="collapse" data-parent="#accordion">
    <div class="card-body">
    <ul style="list-style-type:none;">
    @if($hr > 0)
        @foreach($files as $file)
            @if($file->category == "HR")
            <li>
                <input class="check_file" type="checkbox" id="{{$file->id}}">
                <a rel="nofollow" id="{{$file->name}}" href="{{ url('/download').'/'.$file->name }}"> {{ $file->name }}</a>
                <span class="float-right"><i onClick="delete_click('{{$file->id}}')" class="trash fa fa-trash text-danger"></i></span>
            </li>
            @endif
        @endforeach
        <br>
        <button id="all_hr" onclick="download_all('hr')" class="btn btn-sm btn-danger">All Files Download</button><br>

    @else
    No File 
    @endif
        </ul>
    </div>
  </div>
</div>


<div class="card">
  <div class="card-header">
    <a class="collapsed card-link" data-toggle="collapse" href="#collapsePermits">
      Permits
    </a>
  </div>
  <div id="collapsePermits" class="collapse" data-parent="#accordion">
    <div class="card-body">
    <ul style="list-style-type:none;">
    @if($permits > 0)
    @foreach($files as $file)
            @if($file->category == "Permits")
            <li>
                <input class="check_file" type="checkbox" id="{{$file->id}}">
                <a rel="nofollow" id="{{$file->name}}"href="{{ url('/download').'/'.$file->name }}"> {{ $file->name }}</a>
                <span class="float-right"><i onClick="delete_click('{{$file->id}}')" class="trash fa fa-trash text-danger"></i></span>
            </li>
            @endif
        @endforeach
        <br>
        <button id="all_permits" onclick="download_all('permits')" class="btn btn-sm btn-danger">All Files Download</button><br>        
  
    @else
    No File 
    @endif
        </ul>
    </div>
  </div>
</div>


<div class="card">
  <div class="card-header">
    <a class="collapsed card-link" data-toggle="collapse" href="#collapseOperations">
      Operations
    </a>
  </div>
  <div id="collapseOperations" class="collapse" data-parent="#accordion">
    <div class="card-body">
    <ul style="list-style-type:none;">
    @if($operations > 0)
        @foreach($files as $file)
            @if($file->category == "Operations")
            <li>
                <input class="check_file" type="checkbox" id="{{$file->id}}">
                <a rel="nofollow" id="{{$file->name}}" href="{{ url('/download').'/'.$file->name }}"> {{ $file->name }}</a>
                <span class="float-right"><i onClick="delete_click('{{$file->id}}')" class="trash fa fa-trash text-danger"></i></span>
            </li>
            @endif
        @endforeach
        <br>
        <button id="all_operations" onclick="download_all('opertaions')" class="btn btn-sm btn-danger">All Files Download</button><br>        
    @else
    No File 
    @endif
        </ul>
    </div>
  </div>
</div>


<div class="card">
  <div class="card-header">
    <a class="collapsed card-link" data-toggle="collapse" href="#collapseEstablishment">
      Establishment
    </a>
  </div>
  <div id="collapseEstablishment" class="collapse" data-parent="#accordion">
    <div class="card-body">
    <ul style="list-style-type:none;">
    @if($establishment > 0)
        @foreach($files as $file)
            @if($file->category == "Establishment")
            <li>
                <input class="check_file" type="checkbox" id="{{$file->id}}">
                <a rel="nofollow" id="{{$file->name}}" href="{{ url('/download').'/'.$file->name }}"> {{ $file->name }}</a>
                <span class="float-right"><i onClick="delete_click('{{$file->id}}')" class="trash fa fa-trash text-danger"></i></span>
            </li>
            @endif
        @endforeach
        <br>
        <button id="all_establishment" onclick="download_all('establishment')" class="btn btn-sm btn-danger">All Files Download</button><br>        
    @else
    No File 
    @endif
        </ul>
    </div>
  </div>
</div>


</div>


            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
          <!-- Logout Modal-->
  <div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the product.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>



@endsection

@section('scripts')
      <script>
          $("#alert").fadeTo(2000, 500).slideUp(500, function(){
             $("#alert").slideUp(600);
          });
          
          function delete_click(clicked_id){
            $('#deleteModel').attr("href","{{url('deleteFile')}}/"+clicked_id)
            $('#deleteProductModal').modal('show');
          }

          $('#download_files').click(function(){
            var check = 0;
            var names = [];
            $('input:checkbox.check_file').each(function () {
              var sThisVal = (this.checked ? $(this).val() : "");
              if(sThisVal == "on"){
                  window.open($(this).next('a').attr('href'));
                //  names.push($(this).next('a').attr('id'));
                 check = 1;
              }
             });
             if(check == 0){
               alert("Please Select Some Files First");
             }else{
              //  var url = "{{ url('zipFiles') }}"+'/'+names;
              //   window.open(url);
             }
          });

          function download_all(files){
            var url = "{{ url('download_all') }}"+'/'+files;
            window.open(url);
          }
      </script>
@endsection
