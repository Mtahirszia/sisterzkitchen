@extends('layouts.main')

@section('title' , 'Edit Revenue')

@section('styles')
    <style>
      .add-revenue-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .add-revenue-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

<form  class="add-revenue-form" enctype="multipart/form-data"
 action="{{url('editRevenue')}}" id="add_revenue" method="post">
@csrf

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

<input type="hidden" name="id" value="{{$revenue->id}}" />
  <div class="form-group">
    <label for="productInput">Revenue Name</label>
    <input type="text" name="name" class="form-control" value="{{$revenue->name}}" aria-describedby="revenueNameHelp" placeholder="Enter revenue name">
    @if($errors->has('name'))
    <small id="revenueNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="revenueAmount">Revenue Amount</label>
    <input type="text" name="amount" class="form-control" value="{{$revenue->amount}}" id="revenueAmount" aria-describedby="revenueAmountHelp" placeholder="Enter revenue amount">
    @if($errors->has('amount'))
    <small id="revenueAmountHelp" class="form-text error">{{ $errors->first('amount') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="revenueDescription">Revenue Description</label>
    <textarea type="text" rows="5" name="description" class="form-control" id="revenueDescription" aria-describedby="revenueDescriptionHelp" placeholder="Enter revenue description">{{ $revenue->description }}</textarea>
    @if($errors->has('description'))
    <small id="revenueDescriptionHelp" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="revenueDate">Revenue Date</label>
    <input type="date" rows="5" name="date" class="form-control" value="{{ Carbon\Carbon::parse($revenue->date)->format('Y-m-d') }}" id="revenueDate" aria-describedby="revenueDateHelp" placeholder="Enter revenue date">
    @if($errors->has('date'))
    <small id="revenueDateHelp" class="form-text error">{{ $errors->first('date') }}</small>
    @endif
  </div>

  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

@section('scripts')
@endsection