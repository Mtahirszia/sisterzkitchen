@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            Sisterz Kitchen
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} Sisterz Kitchen . @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
