@extends('layouts.main')

@section('title' , 'Add Product')

@section('styles')
    <style>
      .add-product-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .add-product-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-product-form" enctype="multipart/form-data"
 action="{{route('products.store')}}" id="add_product" method="post">
@csrf

  <div class="form-group">
    <label for="productInput">Product Name</label>
    <input type="text" name="name" class="form-control" id="productInput" value="{{old('name')}}" aria-describedby="productNameHelp" placeholder="Enter product name">
    @if($errors->has('name'))
    <small id="productNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productShortInput">Product Short Name</label>
    <input type="text" name="short_name" class="form-control" id="productShortInput" value="{{old('short_name')}}" aria-describedby="productShortNameHelp" placeholder="Enter product short name">
    @if($errors->has('short_name'))
    <small id="productShortNameHelp" class="form-text error">{{ $errors->first('short_name') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="productPrice">Product Price</label>
    <input type="text" name="price" class="form-control" {{old('price')}} id="productPrice" aria-describedby="productPriceHelp" placeholder="Enter product price">
    @if($errors->has('price'))
    <small id="productPriceHelp" class="form-text error">{{ $errors->first('price') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Unit</label>
    <select name="unit" class="form-control" id="">
      <option value="Kg">Kg</option>
      <option value="Dozen">Dozen</option>
    </select>
    @if($errors->has('unit'))
    <small id="productUnitHelp" class="form-text error">{{ $errors->first('unit') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" checked>
        <label class="custom-control-label" for="customRadio2">Approve</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block">
        <label class="custom-control-label" for="customRadio1">Block</label>
      </div>
  </div>

  <label for="">Visible on</label>
  <div class="form-group">
    <div class="custom-control custom-checkbox custom-control-inline">
        <input type="checkbox" class="custom-control-input" id="web" name="web">
        <label class="custom-control-label" for="web">Web</label>
    </div>

    <div class="custom-control custom-checkbox custom-control-inline">
        <input type="checkbox" class="custom-control-input" id="mob" name="mob">
        <label class="custom-control-label" for="mob">Mobile</label>
      </div>

    <div class="custom-control custom-checkbox custom-control-inline">
      <input type="checkbox" class="custom-control-input" id="adm" name="adm">
      <label class="custom-control-label" for="adm">Admin</label>
    </div>
  </div>

  <div class="form-group">
        <input type="file" name="image[]"  multiple>
        @if($errors->has('image'))
          <small id="productImageHelp" class="form-text error">{{ $errors->first('image') }}</small>
        @endif
  </div>    

  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

@section('scripts')
@endsection