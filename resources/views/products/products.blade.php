@extends('layouts.main')

@section('title' , 'Products')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .small-h3{
        font-size:18px;
        font-weight:bold;
      }
      #toggle_event_editing{
        background:white !important;
      }

    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

@if(session()->has('errorProducts'))
    <div id="alert" class="alert alert-danger">
        {{ session()->get('errorProducts') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Products </h1>
          <p class="mb-4">You can Add , Edit and Delete Products from there. <span class="float-right"><span><i class="fa fa-edit text-danger"></i></span> for Update <span><i class="fa fa-trash text-danger"></i></span> for Delete </span></p>
          <!-- DataTales Example -->
          <div class="mt-5 card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-danger">Products 
                <span class="float-right">
                  <a href="{{url('products/create')}}" title="Add Product"><button class="btn btn-sm btn-danger">Add Product</button></a>
                </span>
               </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="productTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Image</th>
                      <th>Name</th>
                      <th>S Name</th>
                      <th>Price</th>
                      <th>Unit</th>
                      <th>Block</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>S Name</th>
                        <th>Price</th>
                        <th>Unit</th>
                        <th>Block</th>
                        <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->




          <!-- Logout Modal-->
  <div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the product.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>



@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

       function delete_click(clicked_id){
          $('#deleteModel').attr("href","{{url('deleteProduct')}}/"+clicked_id)
          $('#deleteProductModal').modal('show');
        }

        $(document).ready(function(){
            $('#productTable').DataTable({
              "processing":true,
              "serverside":true,
              "ajax":"{{route('ajaxProducts')}}",
              "columns":[
                {"data" : "id"},
                {"data" : "image"},
                {"data" : "name"},
                {"data" : "short_name"},
                {"data" : "price"},
                {"data" : "unit"},
                {"data" : "block"},
                {"data" : "action"}
              ]
            });
        });
  </script>
@endsection
