-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 30, 2020 at 01:34 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageUrl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=180 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `imageUrl`, `product_id`, `created_at`, `updated_at`) VALUES
(1, '', 'https://picsg.files.wordpress.com/2012/11/new-neckline-dress-designs-by-dareecha-embroidered-kashmiri-winter-dress-collection-2013.jpg', 2, NULL, NULL),
(2, '', 'http://1.bp.blogspot.com/-PY7toyKpxVQ/UEtLrR6aNkI/AAAAAAAAAJs/XnsR79OkTMc/s1600/IMAG4183.jpg', 2, NULL, NULL),
(3, '', 'https://mehndidresses.pk/wp-content/uploads/2019/10/best-Kashmiri-Embroidery-Dresses.jpg', 2, NULL, NULL),
(4, '', 'https://cdn.sanaullastore.com/media/catalog/product/k/k/kk-702_green_2_.jpg', 2, NULL, NULL),
(5, '', 'https://mehndidresses.pk/wp-content/uploads/2019/10/Traditional-Kashmiri-Folk-Dance-outfits.jpg', 2, NULL, NULL),
(135, '1580298957.jpg', 'http://192.168.155.109/rajaa/public/images/1580298957.jpg', 112, '2020-01-29 06:55:57', '2020-01-29 06:55:57'),
(136, '1580298957.jpg', 'http://192.168.155.109/rajaa/public/images/1580298957.jpg', 112, '2020-01-29 06:55:57', '2020-01-29 06:55:57'),
(137, '1580299006.jpg', 'http://192.168.155.109/rajaa/public/images/1580299006.jpg', 113, '2020-01-29 06:56:46', '2020-01-29 06:56:46'),
(138, '1580299006.jpg', 'http://192.168.155.109/rajaa/public/images/1580299006.jpg', 113, '2020-01-29 06:56:46', '2020-01-29 06:56:46'),
(139, '1580299779.jpg', 'http://192.168.155.109/rajaa/public/images/1580299779.jpg', 114, '2020-01-29 07:09:39', '2020-01-29 07:09:39'),
(140, '1580299779.jpg', 'http://192.168.155.109/rajaa/public/images/1580299779.jpg', 114, '2020-01-29 07:09:39', '2020-01-29 07:09:39'),
(178, '1580305007.jpg', 'http://192.168.155.109/rajaa/public/images/1580305007.jpg', 117, '2020-01-29 08:36:47', '2020-01-29 08:36:47'),
(179, '1580305007.jpg', 'http://192.168.155.109/rajaa/public/images/1580305007.jpg', 117, '2020-01-29 08:36:47', '2020-01-29 08:36:47'),
(134, '1580298830.jpg', 'http://192.168.155.109/rajaa/public/images/1580298830.jpg', 110, '2020-01-29 06:53:50', '2020-01-29 06:53:50'),
(133, '1580298830.jpg', 'http://192.168.155.109/rajaa/public/images/1580298830.jpg', 110, '2020-01-29 06:53:50', '2020-01-29 06:53:50'),
(132, '1580298648.jpg', 'http://192.168.155.109/rajaa/public/images/1580298648.jpg', 108, '2020-01-29 06:50:48', '2020-01-29 06:50:48'),
(125, '1580297914.jpg', 'http://192.168.155.109/rajaa/public/images/1580297914.jpg', 102, '2020-01-29 06:38:34', '2020-01-29 06:38:34'),
(126, '1580297914.jpg', 'http://192.168.155.109/rajaa/public/images/1580297914.jpg', 102, '2020-01-29 06:38:34', '2020-01-29 06:38:34'),
(127, '1580298272.jpg', 'http://192.168.155.109/rajaa/public/images/1580298272.jpg', 105, '2020-01-29 06:44:32', '2020-01-29 06:44:32'),
(128, '1580298272.jpg', 'http://192.168.155.109/rajaa/public/images/1580298272.jpg', 105, '2020-01-29 06:44:32', '2020-01-29 06:44:32'),
(129, '1580298465.jpg', 'http://192.168.155.109/rajaa/public/images/1580298465.jpg', 106, '2020-01-29 06:47:45', '2020-01-29 06:47:45'),
(130, '1580298465.jpg', 'http://192.168.155.109/rajaa/public/images/1580298465.jpg', 106, '2020-01-29 06:47:45', '2020-01-29 06:47:45'),
(131, '1580298648.jpg', 'http://192.168.155.109/rajaa/public/images/1580298648.jpg', 108, '2020-01-29 06:50:48', '2020-01-29 06:50:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=167 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(154, '2014_10_12_000000_create_users_table', 2),
(155, '2014_10_12_100000_create_password_resets_table', 2),
(156, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(157, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(158, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(159, '2016_06_01_000004_create_oauth_clients_table', 2),
(160, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(161, '2018_09_21_084108_create_products_table', 2),
(162, '2018_09_21_084123_create_orders_table', 2),
(163, '2018_09_21_084135_create_order_items_table', 2),
(164, '2018_09_21_084148_create_transactions_table', 2),
(165, '2019_01_14_073135_add_order_id_to_transactions_table', 2),
(25, '2020_01_19_121103_images', 1),
(166, '2020_01_19_121103_image', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('d4ba406033941412509db6d6ea285551ac6ed6da791673e236276f95e1226f88abfa965bc3405742', 1, 1, 'MyApp', '[]', 0, '2020-01-28 11:43:57', '2020-01-28 11:43:57', '2021-01-28 16:43:57'),
('94352b931882b478fe323c62253d94408b821c5ec002de78fa5a6c26ef7c94705d73851fce589c9c', 1, 1, 'MyApp', '[]', 0, '2020-01-28 11:48:29', '2020-01-28 11:48:29', '2021-01-28 16:48:29'),
('9a722986b4cc238733dfeb0a0710edc86351bf631ba4497d799b0f399cd8027a8fcf2cc922757a58', 1, 1, 'MyApp', '[]', 0, '2020-01-28 11:51:03', '2020-01-28 11:51:03', '2021-01-28 16:51:03'),
('554aec1ba18a6d5c556e700e9e7020ac75df011cdf6cf88c8f62ecbf8b0a361267a7a39eff98fcdc', 1, 1, 'MyApp', '[]', 0, '2020-01-28 11:54:50', '2020-01-28 11:54:50', '2021-01-28 16:54:50'),
('7f4be56e0a8baefc55f3de1a4e143a07c663d2d65f671340c37889951e3b2ed3a47aedc35b4b26d8', 1, 1, 'MyApp', '[]', 0, '2020-01-28 11:56:26', '2020-01-28 11:56:26', '2021-01-28 16:56:26'),
('210308575bef92c72670668f817b72b09f1cff7285b28a9c6712b924b7d3ce0dbd7eb394119bf4e5', 1, 1, 'MyApp', '[]', 0, '2020-01-28 12:01:00', '2020-01-28 12:01:00', '2021-01-28 17:01:00'),
('876e9c56524d84a01e6b247e8d8c5d23392716280b678c71edb8e424494fba8148456c32c7a95e95', 1, 1, 'MyApp', '[]', 0, '2020-01-28 15:04:22', '2020-01-28 15:04:22', '2021-01-28 20:04:22'),
('a80ebc7e90a4547eec5f4348daa94fe8a265c174cddb0c2f59f8893910f0fecbe0f200fdcd08c1f7', 1, 1, 'MyApp', '[]', 0, '2020-01-28 16:58:28', '2020-01-28 16:58:28', '2021-01-28 21:58:28'),
('21e916774c7deef7dd3cd95649a268c7075ad8e4f1c1b7a5f55bd8a4fc19b789af99eb49fb9d1867', 1, 1, 'MyApp', '[]', 0, '2020-01-29 03:46:33', '2020-01-29 03:46:33', '2021-01-29 08:46:33'),
('ee7045cebc287795175e77d547ab1d7da25db43a0711c97332d96fae5fd39f6303f975857b48c87f', 1, 1, 'MyApp', '[]', 0, '2020-01-29 06:11:38', '2020-01-29 06:11:38', '2021-01-29 11:11:38'),
('a834c78469fd8c61e30d729989d175479af1148894560901b95e672ca64e31ae944c50c576d21fe9', 1, 1, 'MyApp', '[]', 0, '2020-01-29 07:38:34', '2020-01-29 07:38:34', '2021-01-29 12:38:34'),
('7aab0901d7c4cb02560efc69326e404cc924f097544b6bf4259476aade6d010fa1deaa1a8d23bda5', 1, 1, 'MyApp', '[]', 0, '2020-01-29 10:02:00', '2020-01-29 10:02:00', '2021-01-29 15:02:00'),
('5f267eeaa2cb9d7377ec03eceb74b43f125162990eccbde07f1b9edaa99a04fd10de0b304c7562f3', 1, 1, 'MyApp', '[]', 0, '2020-01-29 11:33:04', '2020-01-29 11:33:04', '2021-01-29 16:33:04'),
('cc2070a28734c7f74d3400e0194c27f6ebd58061fa31a47e7c19a012d9e35170e8154a8e07cfbbcb', 1, 1, 'MyApp', '[]', 0, '2020-01-29 11:40:57', '2020-01-29 11:40:57', '2021-01-29 16:40:57'),
('a8706f7bae2d2f0351b1e532f8db3630a2b82ba6f3b342c2629a153cecd6d2ddc79ae3744d62546a', 1, 1, 'MyApp', '[]', 0, '2020-01-29 13:12:58', '2020-01-29 13:12:58', '2021-01-29 18:12:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'SnizwysHqpCUjNa7s3kGePEvt9LjaDyDsl7hL6rN', 'http://localhost', 1, 0, 0, '2020-01-28 11:42:13', '2020-01-28 11:42:13'),
(2, NULL, 'Laravel Password Grant Client', '9SihUOAkQlMIPGPhPR6rXrIJFyxyRHqG3vDrpD40', 'http://localhost', 0, 1, 0, '2020-01-28 11:42:13', '2020-01-28 11:42:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-01-28 11:42:13', '2020-01-28 11:42:13');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `status` enum('NEW','PROCESSING','FAILED','COMPLETED') COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `amount`, `status`, `street`, `house`, `area`, `city`, `created_at`, `updated_at`) VALUES
(1, 1, 1125.00, 'COMPLETED', 'street 1', 'house 952/25', 'Azizabad', 'Rawalpindi', '2020-01-28 11:51:36', '2020-01-29 08:59:36'),
(2, 1, 1383.00, 'PROCESSING', NULL, NULL, NULL, NULL, '2020-01-29 07:30:36', '2020-01-29 07:30:36'),
(3, 1, 508.00, 'PROCESSING', NULL, NULL, NULL, NULL, '2020-01-29 07:32:34', '2020-01-29 07:32:34'),
(4, 1, 508.00, 'PROCESSING', 'Street 1', 'House 952/25', 'Azizabad', 'Rawalpindi k', '2020-01-29 07:34:23', '2020-01-29 07:34:23'),
(5, 1, 1233.00, 'PROCESSING', 'abc', 'bbc', 'ddc', 'eec', '2020-01-29 07:38:59', '2020-01-29 07:38:59'),
(6, 1, 1233.00, 'PROCESSING', 'abc', 'bbc', 'ddc', 'eec', '2020-01-29 07:40:27', '2020-01-29 07:40:27'),
(7, 1, 1233.00, 'COMPLETED', 'abc', 'bbc', 'ddc', 'eec', '2020-01-29 07:41:43', '2020-01-29 07:44:54'),
(8, 1, 762.00, 'PROCESSING', 'abc', 'bbc', 'ddc', 'eec', '2020-01-29 07:46:17', '2020-01-29 07:46:17'),
(9, 1, 1225.00, 'COMPLETED', 'ggg', 'hhh', 'jjj', 'kkk', '2020-01-29 10:58:37', '2020-01-29 11:00:22'),
(10, 1, 400.00, 'PROCESSING', 'ggg', 'hhh', 'jjj', 'kkk', '2020-01-29 10:59:43', '2020-01-29 10:59:43'),
(11, 1, 900.00, 'PROCESSING', 'ggg', 'hhh', 'jjj', 'kkk', '2020-01-29 11:05:41', '2020-01-29 11:05:41'),
(12, 1, 450.00, 'PROCESSING', 'ggg', 'hhh', 'jjj', 'kkk', '2020-01-29 11:22:24', '2020-01-29 11:22:24'),
(13, 1, 1215.00, 'PROCESSING', 'uio', 'hgf', 'vbb', 'dff', '2020-01-29 11:51:17', '2020-01-29 11:51:17'),
(14, 1, 300.00, 'PROCESSING', 'uio', 'hgf', 'vbb', 'dff', '2020-01-29 11:54:01', '2020-01-29 11:54:01'),
(15, 1, 800.00, 'PROCESSING', 'uio', 'hgf', 'vbb', 'dff', '2020-01-29 11:54:35', '2020-01-29 11:54:35'),
(16, 1, 500.00, 'COMPLETED', 'uio', 'hgf', 'vbb', 'dff', '2020-01-29 11:57:26', '2020-01-29 13:13:12'),
(17, 1, 1180.00, 'PROCESSING', 'uio', 'hgf', 'vbb', 'dff', '2020-01-29 12:00:56', '2020-01-29 12:00:56'),
(18, 1, 150.00, 'PROCESSING', 'uio', 'hgf', 'vbb', 'dff', '2020-01-29 12:01:14', '2020-01-29 12:01:14'),
(19, 1, 675.00, 'PROCESSING', 'uio', 'hgf', 'vbb', 'dff', '2020-01-29 12:27:57', '2020-01-29 12:27:57'),
(20, 1, 1800.00, 'PROCESSING', 'uio', 'hgf', 'vbb', 'dff', '2020-01-30 05:18:06', '2020-01-30 05:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `quantity`, `amount`, `created_at`, `updated_at`) VALUES
(1, '1', 2, 1, 500.00, '2020-01-28 11:51:36', '2020-01-28 11:51:36'),
(2, '1', 1, 1, 225.00, '2020-01-28 11:51:36', '2020-01-28 11:51:36'),
(3, '1', 4, 1, 400.00, '2020-01-28 11:51:36', '2020-01-28 11:51:36'),
(4, '2', 1, 1, 225.00, '2020-01-29 07:30:36', '2020-01-29 07:30:36'),
(5, '2', 2, 1, 500.00, '2020-01-29 07:30:36', '2020-01-29 07:30:36'),
(6, '2', 3, 1, 150.00, '2020-01-29 07:30:36', '2020-01-29 07:30:36'),
(7, '2', 115, 2, 508.00, '2020-01-29 07:30:36', '2020-01-29 07:30:36'),
(8, '3', 115, 2, 508.00, '2020-01-29 07:32:34', '2020-01-29 07:32:34'),
(9, '4', 115, 2, 508.00, '2020-01-29 07:34:23', '2020-01-29 07:34:23'),
(10, '5', 1, 1, 225.00, '2020-01-29 07:38:59', '2020-01-29 07:38:59'),
(11, '5', 2, 1, 500.00, '2020-01-29 07:38:59', '2020-01-29 07:38:59'),
(12, '5', 115, 2, 508.00, '2020-01-29 07:38:59', '2020-01-29 07:38:59'),
(13, '6', 1, 1, 225.00, '2020-01-29 07:40:27', '2020-01-29 07:40:27'),
(14, '6', 2, 1, 500.00, '2020-01-29 07:40:27', '2020-01-29 07:40:27'),
(15, '6', 115, 2, 508.00, '2020-01-29 07:40:27', '2020-01-29 07:40:27'),
(16, '7', 1, 1, 225.00, '2020-01-29 07:41:43', '2020-01-29 07:41:43'),
(17, '7', 2, 1, 500.00, '2020-01-29 07:41:43', '2020-01-29 07:41:43'),
(18, '7', 115, 2, 508.00, '2020-01-29 07:41:43', '2020-01-29 07:41:43'),
(19, '8', 115, 3, 762.00, '2020-01-29 07:46:17', '2020-01-29 07:46:17'),
(20, '9', 2, 2, 1000.00, '2020-01-29 10:58:37', '2020-01-29 10:58:37'),
(21, '9', 1, 1, 225.00, '2020-01-29 10:58:37', '2020-01-29 10:58:37'),
(22, '10', 4, 1, 400.00, '2020-01-29 10:59:43', '2020-01-29 10:59:43'),
(23, '11', 4, 1, 400.00, '2020-01-29 11:05:41', '2020-01-29 11:05:41'),
(24, '11', 2, 1, 500.00, '2020-01-29 11:05:41', '2020-01-29 11:05:41'),
(25, '12', 1, 2, 450.00, '2020-01-29 11:22:24', '2020-01-29 11:22:24'),
(26, '13', 117, 1, 115.00, '2020-01-29 11:51:17', '2020-01-29 11:51:17'),
(27, '13', 2, 1, 500.00, '2020-01-29 11:51:17', '2020-01-29 11:51:17'),
(28, '13', 5, 2, 600.00, '2020-01-29 11:51:17', '2020-01-29 11:51:17'),
(29, '14', 5, 1, 300.00, '2020-01-29 11:54:01', '2020-01-29 11:54:01'),
(30, '15', 5, 1, 300.00, '2020-01-29 11:54:35', '2020-01-29 11:54:35'),
(31, '15', 2, 1, 500.00, '2020-01-29 11:54:35', '2020-01-29 11:54:35'),
(32, '16', 2, 1, 500.00, '2020-01-29 11:57:26', '2020-01-29 11:57:26'),
(33, '17', 1, 2, 450.00, '2020-01-29 12:00:56', '2020-01-29 12:00:56'),
(34, '17', 2, 1, 500.00, '2020-01-29 12:00:56', '2020-01-29 12:00:56'),
(35, '17', 117, 2, 230.00, '2020-01-29 12:00:56', '2020-01-29 12:00:56'),
(36, '18', 6, 1, 150.00, '2020-01-29 12:01:14', '2020-01-29 12:01:14'),
(37, '19', 1, 3, 675.00, '2020-01-29 12:27:57', '2020-01-29 12:27:57'),
(38, '20', 5, 1, 300.00, '2020-01-30 05:18:06', '2020-01-30 05:18:06'),
(39, '20', 2, 3, 1500.00, '2020-01-30 05:18:06', '2020-01-30 05:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promotion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `image`, `promotion`, `created_at`, `updated_at`) VALUES
(1, 'Women Cloth', 225.00, 'https://ae01.alicdn.com/kf/Hc5eb27d1d1de4d1898375949db2ae885z/Summer-Lace-Patchwork-V-Neck-Sexy-Women-Cloth-Bodycon-Sheath-Pencil-Knee-Length-Party-White-Dress.jpg_640x640.jpg', 'no', NULL, NULL),
(2, 'Kashmiri Frock', 500.00, 'https://3.imimg.com/data3/PO/YM/MY-19102226/women-cloth-500x500.jpeg', 'no', NULL, NULL),
(3, 'Women Purple', 150.00, 'https://i.pinimg.com/736x/2a/d1/9d/2ad19d20e987356ba041f67fcda5c80b.jpg', 'no', NULL, NULL),
(4, 'Long Sleeve', 400.00, 'https://sc02.alicdn.com/kf/HTB1AtHsNpXXXXXVXpXXq6xXFXXXj/OEM-3-4-Long-Sleeve-V-Neck.jpg_350x350.jpg', 'yes', NULL, NULL),
(5, 'Women Formal Dress', 300.00, 'https://i.ebayimg.com/images/g/GEcAAOSwWUBcNYiA/s-l300.jpg', 'yes', NULL, NULL),
(6, 'Women Latest', 150.00, 'https://i.pinimg.com/originals/e1/85/02/e18502779f35cae220e4baf279f25421.jpg', 'no', '2020-01-23 02:12:28', '2020-01-23 02:12:28'),
(117, 'Zafar Don', 115.00, 'http://192.168.155.109/rajaa/public/images/1580305007.jpg', 'no', '2020-01-29 08:36:16', '2020-01-29 08:36:47');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `raw_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `transaction_id`, `status`, `raw_data`, `created_at`, `updated_at`, `order_id`) VALUES
(1, '72a84b70-41ee-11ea-bd5b-8d610e559d5d', 'COMPLETED', 'abcabc', '2020-01-28 11:51:36', '2020-01-29 08:59:36', '1'),
(2, 'b467fb40-4294-11ea-8d59-e1c5f4ba0d69', 'COMPLETED', 'abc', '2020-01-29 07:41:43', '2020-01-29 07:44:54', '7'),
(3, '57ddb5c0-4295-11ea-a3e7-97b4b950560e', 'PROCESSING', 'abc', '2020-01-29 07:46:17', '2020-01-29 07:46:17', '8'),
(4, '360111d0-42b0-11ea-a58c-0769588c8946', 'COMPLETED', 'abc', '2020-01-29 10:58:37', '2020-01-29 11:00:22', '9'),
(5, '5d4a1ef0-42b0-11ea-8ce3-a17b82c3b907', 'PROCESSING', 'abc', '2020-01-29 10:59:43', '2020-01-29 10:59:43', '10'),
(6, '3283db40-42b1-11ea-a778-57055b27252c', 'PROCESSING', 'abc', '2020-01-29 11:05:41', '2020-01-29 11:05:41', '11'),
(7, '88b0bae0-42b3-11ea-8fc9-c3218c2dbdb0', 'PROCESSING', 'abc', '2020-01-29 11:22:24', '2020-01-29 11:22:24', '12'),
(8, '91ada5c0-42b7-11ea-9588-572ecc79cb50', 'PROCESSING', 'abc', '2020-01-29 11:51:17', '2020-01-29 11:51:17', '13'),
(9, 'f2f851e0-42b7-11ea-86a0-bf797f770271', 'PROCESSING', 'abc', '2020-01-29 11:54:01', '2020-01-29 11:54:01', '14'),
(10, '07c54c90-42b8-11ea-a136-053604e13d01', 'PROCESSING', 'abc', '2020-01-29 11:54:36', '2020-01-29 11:54:36', '15'),
(11, '6da060e0-42b8-11ea-95b2-81bfb1ec02e8', 'COMPLETED', 'abc', '2020-01-29 11:57:26', '2020-01-29 13:13:12', '16'),
(12, 'eacec2f0-42b8-11ea-ae69-b73d5cfa22a8', 'PROCESSING', 'abc', '2020-01-29 12:00:56', '2020-01-29 12:00:56', '17'),
(13, 'f5830110-42b8-11ea-b811-6dc80e81f287', 'PROCESSING', 'abc', '2020-01-29 12:01:14', '2020-01-29 12:01:14', '18'),
(14, 'b0a7ca60-42bc-11ea-bbd3-eb001b4d002c', 'PROCESSING', 'abc', '2020-01-29 12:27:57', '2020-01-29 12:27:57', '19'),
(15, 'ce79fdc0-4349-11ea-bd33-53ce315d8d04', 'PROCESSING', 'abc', '2020-01-30 05:18:06', '2020-01-30 05:18:06', '20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_phone_unique` (`phone`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `isAdmin`, `phone`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Zafar Khan', 1, '03155053968', 'zafarkhan1183@gmail.com', NULL, '$2y$10$b36gQFGXtigu209Aw8CVZ.ReREQSzgTwc4LGcXDGkfX/2lWKDEpey', NULL, '2020-01-28 11:43:56', '2020-01-28 11:43:56');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
